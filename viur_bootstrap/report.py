# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

import os.path
import json
from collections import OrderedDict

import logger


class ReportDomain(object):
	"""Use this class to create a new report domain, like a hull or limb to install, vi or viur"""

	def __init__(self, key, section=None):
		super(ReportDomain, self).__init__()
		self.key = key
		self.installed_files = list()
		self.section = section

	def add_installed_files(self, installed_files):
		"""Adds files or directories to this report domain

		installed files can be only one entry or a list of entries The list will be merged with our
		files already installed.

		:param installed_files: file or list of files
		:type installed_files: :class:`basestring` or :class:`list`
		"""
		if isinstance(installed_files, basestring):
			self.installed_files.append(installed_files)
		else:
			self.installed_files.extend(installed_files)

	def collect_report(self):
		"""This is a callback method for an Report instance.

		If you want to provide more information, you should overwrite this method in your subclass

		:return: a tuple with the key, the report data and the optional section
		:rtype: str, dict, str
		"""
		return self.key, {"installed_files": self.installed_files}, self.section


class Report(object):
	"""This class collects all information about a installation or change of a viur instance

	The result/metadata will be saved as instance/report.json and the class populates itself if the file
	already exists.

	Dictionaries in the json domain will be interpreted as OrderedDicts, so order of objects is important!
	"""

	def __init__(self, config, report=None):
		super(Report, self).__init__()
		self.report = report and report or OrderedDict()
		self.reporters = OrderedDict()
		self.config = config
		self.report_path = os.path.join(self.config["path"], "report.json")
		self.sections = set()

	def update(self, key, data, section=None):
		"""Adds files or directories to the cache of installed per key

		A key is a keyword like hull or limb name

		Will be used to document what was installed or has changed

		:param key: the report domain/key
		:type key: str
		:param data: the report of a ReportMix subclass
		:type data: OrderedDict
		:param section: an optional sub dict called section under which the domain will be stored
		:type section: str
		"""

		try:
			if section:
				self.sections.add(section)
				installed_limbs = self.report.setdefault(section, OrderedDict())
				domain_dict = installed_limbs.setdefault(key, OrderedDict())
			else:
				domain_dict = self.report.setdefault(key, OrderedDict())
			domain_dict.update(data)
		except KeyError:
			logger.logger.error("tried to access unknown key %r in our report", key)

	def collect_reports(self):
		"""Walks through all registered ReportDomains and collects information to put into the report"""

		self.report["config"] = self.config
		for name, report_handler in self.reporters.iteritems():
			try:
				data = report_handler.collect_report()
			except AttributeError:
				data = report_handler.report_domain.collect_report()
			logger.logger.info("collect_reports: %r", data)
			self.update(*data)
		self.to_metadata()

	def iterate_domains(self, git):
		def handle_domain(domain, report, islimb=False):
			try:
				logger.logger.debug("iterating over report domain %r with report %r", domain, report)
				installed_files = list(set(report["installed_files"]))
				git.on_changed(installed_files,
				               "viur_bootstrap: files updated in {0}domain {1!r}:\n{2}\n".format(
					               islimb and "limb " or "", domain, "\n".join(
						               ["  * {0}".format(fp) for fp in report["installed_files"]])))
			except KeyError:
				pass

		for domain, report in self.report.iteritems():
			if domain not in self.sections:
				handle_domain(domain, report)
			else:
				for limb_domain, limb_report in report.iteritems():
					handle_domain(limb_domain, limb_report, True)

	@classmethod
	def from_metadata(cls, config):
		"""Let's see, if there is already a 'report.json' file in config path

		:param cls: The Report class to create
		:param config: the config given by our Driver
		:type config: dict
		:return: a Report instance
		"""
		report_path = os.path.join(config["path"], "report.json")
		report = None
		if os.path.isfile(report_path):
			# print "found installation report - loading report.json..."
			report = json.loads(open(report_path).read(), object_pairs_hook=OrderedDict)
		return cls(config, report)

	def to_metadata(self):
		"""Serializes this report to a report.json"""

		open(self.report_path, "w").write(
			json.dumps(self.report, indent=4))

	def register_domain_handler(self, key, handler):
		self.reporters[key] = handler
