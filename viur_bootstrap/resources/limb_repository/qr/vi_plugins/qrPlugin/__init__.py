import html5
from network import DeferredCall
from priorityqueue import actionDelegateSelector
from i18n import translate


class QRCodeDownloadAction(html5.ext.Button):
	"""
		Allows downloading files from the server.
	"""

	def __init__(self, *args, **kwargs):
		super(QRCodeDownloadAction, self).__init__(translate("QRCode Download"), *args, **kwargs)
		self["class"] = "icon download"
		self["disabled"] = True
		self.isDisabled = True

	def onAttach(self):
		super(QRCodeDownloadAction, self).onAttach()
		self.parent().parent().selectionChangedEvent.register(self)

	def onDetach(self):
		self.parent().parent().selectionChangedEvent.unregister(self)

	def onSelectionChanged(self, table, selection):
		if selection:
			if self.isDisabled:
				self.isDisabled = False
			self["disabled"] = False
		else:
			if not self.isDisabled:
				self["disabled"] = True
				self.isDisabled = True

	@staticmethod
	def isSuitableFor(modul, handler, actionName):
		if modul is None:
			return (False)
		correctAction = actionName == "generateqr"
		correctHandler = handler == "list.qr" or handler.startswith("list.qr.")
		return correctAction and correctHandler

	def onClick(self, sender=None):
		selection = self.parent().parent().getCurrentSelection()
		if not selection:
			return
		backOff = 50
		self.disableViUnloadingWarning()
		for s in selection:
			DeferredCall(self.doDownload, s, _delay=backOff)
			backOff += 50
		# self.doDownload(s)
		DeferredCall(self.enableViUnloadingWarning, _delay=backOff + 1000)


	def disableViUnloadingWarning(self, *args, **kwargs):
		eval("window.top.preventViUnloading = false;")

	def enableViUnloadingWarning(self, *args, **kwargs):
		eval("window.top.preventViUnloading = true;")

	def doDownload(self, skel):
		a = html5.A()
		a["href"] = "/qr/download/%s" % skel["id"]
		a.element.click()


actionDelegateSelector.insert(1, QRCodeDownloadAction.isSuitableFor, QRCodeDownloadAction)
