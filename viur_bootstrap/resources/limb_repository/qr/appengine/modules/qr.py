# -*- coding: utf-8 -*-
import StringIO

import qrcode
import qrcode.image
import qrcode.image.svg

from server.applications.list import List
from server import errors
from server import request
from server import conf


class qr(List):
	listTemplate = "qr_list"
	viewTemplate = "qr_view"
	columns = ["name", "external_id", "descr", "target_url"]

	adminInfo = {"name": "qrcode",
	             "handler": "list.qr",  # Which handler to invoke
	             "icon": "icons/modules/qr_code.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "actions": ["generateqr"],
	             "columns": columns,
	}

	def listFilter(self, filter):
		return filter

	def generate(self, id, **kwargs):
		qr_url = conf["viur.mainApp"].appconf.getContents()["qrcode_url"].value

		try:
			qrversion = int(kwargs["qrversion"])
		except (KeyError, ValueError):
			qrversion = 1

		try:
			qrsize = int(kwargs["qrsize"])
		except (KeyError, ValueError):
			qrsize = 32

		try:
			qrformat = kwargs["qrformat"]
			if qrformat not in ("svg", "png"):
				qrformat = "svg"
		except KeyError:
			qrformat = "svg"

		if qrformat == "svg":
			qrfactory = qrcode.image.svg.SvgPathImage
		else:
			qrfactory = None

		skel = self.viewSkel()
		skel.fromDB(id)
		qr_code = qrcode.QRCode(version=qrversion, error_correction=qrcode.ERROR_CORRECT_L, box_size=qrsize,
		                        image_factory=qrfactory)

		qr_code.add_data("%s/qr/%s" % (qr_url.rstrip("/"), skel["external_id"].value))
		qr_code.make()
		im = qr_code.make_image()
		buf = StringIO.StringIO()
		im.save(buf)
		return buf.getvalue()

	generate.interalExposed = True

	def download(self, id, **kwargs):
		svg = self.generate(id, **kwargs)
		request.current.get().response.headers.add_header("Content-disposition",
		                                                  "attachment; filename=qrcode-%s.svg" % str(id))
		request.current.get().response.clear()
		request.current.get().response.headers['Content-Type'] = "image/svg"
		return svg

	download.exposed = True

	def index(self, code):
		query = self.viewSkel().all()

		query.mergeExternalFilter({"code": code})
		query = self.listFilter(query)
		if query is None:
			raise errors.Unauthorized()
		skel = query.fetch()[0]
		dst = skel["target_url"].value
		raise errors.Redirect(dst)

	index.exposed = True


qrcode.jinja2 = True
