#!/bin/sh
#
# SYNCs the base_hull from ViUR/base.
#

BASEDIR="../../../../base"

if [ ! -d $BASEDIR ]
then
	echo "base repo must exist!"
	exit 0
fi

echo "Updating base_hull..."
rsync -av --delete $BASEDIR/ base_hull/ --exclude={.*,README.md,mksetup.py,server}
echo "Update finished."
