# -*- coding: utf-8 -*-
__author__ = 'stefan'

import json
import os
import os.path
import sys

import logger
import lapi


class Limb(object):
	"""A Limb holds the information needed to install additional features like Skeletons, Modules, render methods
	and static content.

	It's even possible to add new bones to existing skeletons, e.g add needed bones to a appconf singleton.

	The actual features a limb can provide, will be determined by LAPI versions and handled by LAPI handlers.

	For now the current LAPI version is 1, so take a look in :class:`viur_bootstrap.lapi.LAPIHandler1`
	for more information.
	"""

	def __init__(self, driver, metadata, dir_name=None):
		"""ctor for a limb

		will be called e.g. by the factory :meth:`Limb.from_file`.

		The real work will be done by a 'LAPI' handler, which is chosen by the 'LAPI' metadata key describing the
		LAPI VERSION.

		.. Note:
			LAPI means limb api

		:param driver:
		:type driver: :class:`viur_bootstrap.driver.Driver`
		:param metadata: the dictionary with limb metadata
		:type metadata: dict
		:param dir_name: the optional directory name if the limb when its a limb loaded from the file system
		:type dir_name: str
		"""
		super(Limb, self).__init__()
		self.driver = driver
		self.config = driver.config
		self.dir_name = dir_name
		self.git = driver.git
		#: a :class:`dict` instance holding information what to do. Source can be a 'metadata.json' file.
		self.metadata = metadata
		self._lapi_handler = None
		self._load_lapi_handler()

	@staticmethod
	def from_file(driver, dir_name):
		"""loads a limbs metadata and returns a new Limb instance with that metadata set

		:param driver: the driver instance
		:type driver: Driver
		:param dir_name: the directory name of the limb to initialize from
		:type dir_name: str
		:return: Limb
		"""
		try:
			limb_dir = os.path.join(driver.config["limb_repository_path"], dir_name)
			metadata = json.load(
				open(os.path.join(limb_dir, "metadata.json")))
			return Limb(driver, metadata, dir_name)
		except Exception, e:
			logger.logger.exception(e)
			sys.exit(-1)

	def _load_lapi_handler(self):
		"""Internal method to load the actual lapi version handler specified in :attr:`viur_bootstrap.limb.Limb.metadata`
		"""
		lapi_version = self.metadata["LAPI"]
		lapi_handler = "LAPIHandler%d" % lapi_version
		self._lapi_handler = getattr(lapi, lapi_handler)(self.driver, self)
		self._lapi_handler.validate_metadata()

	def install(self):
		"""Calls the install method of the actual lapi version handler
		"""
		self._lapi_handler.install()

	EQUALITY_ATTRIBUTES = ["name", "LAPI", "version"]

	def __ge__(self, other):
		return all([self.metadata[attr] >= other.metadata[attr] for attr in self.EQUALITY_ATTRIBUTES])

	def __gt__(self, other):
		return all([self.metadata[attr] > other.metadata[attr] for attr in self.EQUALITY_ATTRIBUTES])

	def __le__(self, other):
		return all([self.metadata[attr] <= other.metadata[attr] for attr in self.EQUALITY_ATTRIBUTES])

	def __lt__(self, other):
		return all([self.metadata[attr] < other.metadata[attr] for attr in self.EQUALITY_ATTRIBUTES])

	def __eq__(self, other):
		return all([self.metadata[attr] == other.metadata[attr] for attr in self.EQUALITY_ATTRIBUTES])

	def __ne__(self, other):
		return all([self.metadata[attr] != other.metadata[attr] for attr in self.EQUALITY_ATTRIBUTES])
