# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

import logging
import os.path

# singleton objects, will be initialized when the dest path is available, e.g after the config was loaded
logger = None

def get_logger(path):
	"""

	:param path:
	:return: logging.RootLogger
	"""
	global logger
	if logger is None:
		logger = logging.getLogger("root")
		logger.setLevel(logging.DEBUG)
		fh = logging.FileHandler(os.path.join(path, 'viur-bootstrap.log'))
		fh.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
		fh.setFormatter(formatter)
		ch.setFormatter(formatter)
		logger.addHandler(fh)
		logger.addHandler(ch)
	return logger
