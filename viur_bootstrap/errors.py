# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'


class ViurBootstrapException(Exception):
	pass


class HullAlreadyInstalled(Exception):
	pass


class ViurBootstrapError(ViurBootstrapException):
	pass


class ViurBootstrapWarning(ViurBootstrapException):
	pass


class GitNotFoundError(ViurBootstrapError):
	pass
