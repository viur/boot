# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

import sys
import os.path
from shutil import move
import json
from datetime import datetime
from distutils.dir_util import copy_tree

from jsonschema import validate

from errors import HullAlreadyInstalled
import logger
from schema.hull_schema import hull_schema
from report import ReportDomain
import utils


class Hull(object):
	"""A Hull holds all information to initialize a new viur application instance

	The information what to do will be gathered from a metadata.json file.

	There is a json schema specified for hulls with required information and types. Metadata will be validated
	after loading.

	All actions this class provides are supporting a dry_run flag to test, which files would be installed or updated
	without changing anything.

	"""

	def __init__(self, driver, metadata, dir_name=None):
		"""Ctor

		:param driver: foo
		:type driver: Driver
		:param metadata: foo
		:type metadata: dict
		:param dir_name: foo
		:type dir_name: str
		"""
		super(Hull, self).__init__()
		self.report_domain = ReportDomain("hull")
		driver.report.register_domain_handler("hull", self)
		self.driver = driver
		self.metadata = metadata
		self.dir_name = dir_name
		self.config = driver.config
		self.git = driver.git
		self.appengine_exists = self.exists_appengine()
		self.hull_src_path = os.path.join(self.config["hull_repository_path"])
		self.config["user_module"] = self.config["user_type"] == "custom" and "user_custom" or "user_google"
		self.config["user_module_class"] = self.config["user_type"] == "custom" and "CustomUser" or "GoogleUser"
		self.repository = None

	@staticmethod
	def from_file(parent, name):
		try:
			metadata = json.load(
				open(os.path.join(parent.config["hull_repository_path"], name, "metadata.json")))
			try:
				validate(metadata, hull_schema)
			except Exception, err:
				logger.logger.exception(err)
				sys.exit(-1)
			return Hull(parent, metadata, name)
		except Exception, e:
			logger.logger.exception(e)
			sys.exit(-1)

	@staticmethod
	def from_metadata(driver, metadata):
		validate(metadata, hull_schema)
		return Hull(driver, metadata)

	def renaming_targets(self, dry_run=False):
		"""This task renames files inside appengine_path

		The files must use :meth:`str.format()` string replacement syntax.

		The keywords for formatting are specified in the metadata and uses the corresponding
		values from Driver.config

		:param dry_run:
		:return:
		"""
		for task in self.metadata["renaming_targets"]:
			src_name = task["src_name"]
			dst_fmt = task["dst_fmt"]
			dst_args = task["dst_args"]
			config_args = [self.config[arg] for arg in dst_args]
			src = os.path.abspath(os.path.join(self.config["appengine_path"], src_name))
			dst = os.path.abspath(os.path.join(self.config["appengine_path"], dst_fmt.format(*config_args)))
			if not dry_run:
				move(src, dst)

			utils.replace_keywords(dst, **self.config)

			self.report_domain.installed_files.append(dst)
			try:
				self.report_domain.installed_files.remove(src)
			except ValueError:
				pass

	def collect_report(self):
		"""This is a callback method for an Report instance.

		If you want to provide more information, you should overwrite this method in your subclass

		:return: a tuple with the key, the report data and if this mixin is a limb
		:rtype: str, dict, bool
		"""
		self.metadata["updated"] = datetime.now().strftime("%Y.%m.%d %H:%M")
		self.metadata.setdefault("created", self.metadata["updated"])
		self.metadata["repository"] = self.driver.hull_repository.metadata["name"]
		return self.report_domain.key, {"installed_files": self.report_domain.installed_files,
		                                "metadata": self.metadata,
		                                "name": self.dir_name}, self.report_domain.section

	def template_targets(self, dry_run=False):
		"""Opens files in appengine path as templates and formats its content.

		Content will be inserted from the configuration at ``{{placeholder}}``
		positions.

		>>> "python code...{{a_config_value}} python code...{{other_config_value}}"

		The keywords for formatting are specified in the metadata and uses the corresponding
		values from :data:`Driver.config`

		:param dry_run: only pretend to do and see what's going to be changed or added
		:type dry_run: bool
		"""
		for file_name in self.metadata["template_targets"]:
			file_name = os.path.join(self.config["appengine_path"], file_name)
			logger.logger.info("processing: {0}".format(file_name))
			if not dry_run:
				utils.replace_keywords(file_name, **self.config)

			self.report_domain.installed_files.append(file_name)

	def exists_appengine(self):
		"""Checks if there is already an existing appengine directory.

		:return: bool
		"""

		return os.path.isdir(self.config["appengine_path"])

	def install(self, dry_run=False):
		"""Applies all steps of hull installation to a provided path, can also be a dry run

		:param dry_run: only pretend to do and see what's going to be changed or added
		:type dry_run: bool
		:return:
		"""
		if self.appengine_exists:
			logger.logger.info("Aborting hull command 'install' - appengine directory %r exists",
			                   self.config["appengine_path"])
			raise HullAlreadyInstalled()

		logger.logger.info("Entering hull command 'install'...")
		logger.logger.info("Installing viur hull %r to %r...", self.dir_name, self.config["appengine_path"])
		self.report_domain.add_installed_files(
			copy_tree(os.path.join(self.config["hull_repository_path"], self.dir_name),
			          self.config["appengine_path"], dry_run))
		self.renaming_targets(dry_run)
		self.template_targets(dry_run)

	EQUALITY_ATTRIBUTES = ["name", "version"]

	def __ge__(self, other):
		return all([getattr(self, attr) >= getattr(other, attr) for attr in self.EQUALITY_ATTRIBUTES])

	def __gt__(self, other):
		return all([getattr(self, attr) > getattr(other, attr) for attr in self.EQUALITY_ATTRIBUTES])

	def __le__(self, other):
		return all([getattr(self, attr) <= getattr(other, attr) for attr in self.EQUALITY_ATTRIBUTES])

	def __lt__(self, other):
		return all([getattr(self, attr) < getattr(other, attr) for attr in self.EQUALITY_ATTRIBUTES])

	def __eq__(self, other):
		return all([getattr(self, attr) == getattr(other, attr) for attr in self.EQUALITY_ATTRIBUTES])

	def __ne__(self, other):
		return all([getattr(self, attr) != getattr(other, attr) for attr in self.EQUALITY_ATTRIBUTES])
