# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

import os.path

import logger
from report import ReportDomain


class Viur(object):
	def __init__(self, driver):
		"""

		:param driver: our master control program
		:type driver: :class:`viur_bootstrap.driver.Driver`
		"""
		super(Viur, self).__init__()

		self.report_domain = ReportDomain("viur")
		driver.report.register_domain_handler("viur", self)
		self.driver = driver
		self.config = driver.config
		self.git = driver.git
		self.viur_add = self.config.setdefault("vi_add", False)
		self.viur_path = os.path.join(self.config["appengine_path"], "server")
		self.added = self.exists()

	def exists(self):
		return os.path.isdir(self.viur_path)

	def add(self):
		logger.logger.info("checking out viur into %r...", self.config["path"])
		if self.config["viur_add"] and not self.added:
			if self.config["is_git_repo"] and self.config["viur_submodule"]:
				self.git.add_submodule(self.config["viur_url"], self.config["appengine_path"])
				self.report_domain.add_installed_files(self.viur_path)
			else:
				self.git.clone(self.config["viur_url"], self.config["appengine_path"])

			self.git.switch_branch(self.config["viur_branch"], self.viur_path)
			self.added = True
