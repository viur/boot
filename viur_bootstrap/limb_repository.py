# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'
import os.path
import json

from jsonschema import validate

from logger import logger
from schema.limb_repository_schema import limb_repository_schema
from formatters.limb_format import LimbMetadataFormatter


class LimbRepository(object):
	"""

	"""

	def __init__(self, driver, metadata):
		super(LimbRepository, self).__init__()
		self.driver = driver
		self.metadata = metadata
		self.config = driver.config
		self.limb_repository_path = self.config["limb_repository_path"]
		self.available_limbs, self.cache = self.retrieve_limbs()

	def search(self, query):
		for limb in self.available_limbs:
			if limb["name"].find(query) != -1:
				LimbMetadataFormatter(limb)()

	@staticmethod
	def from_metadata(parent, path):
		try:
			limb_repository_metadata = json.load(open(os.path.join(path, "metadata.json")))
			validate(limb_repository_metadata, limb_repository_schema)
			return LimbRepository(parent, limb_repository_metadata)
		except Exception, e:
			raise

	def sync(self):
		if self.metadata["git_url"]:
			logger.info("updating limb repository %r", self.metadata["name"])
			self.driver.git.pull(self.limb_repository_path)
		else:
			logger.info("skipping limb repository %r - no upstream url specified", self.metadata["name"])

	def retrieve_limbs(self):
		limbs = list()
		cache = dict()
		for item in os.listdir(self.limb_repository_path):
			directory = os.path.join(self.limb_repository_path, item)
			if os.path.isdir(directory):
				metadata_path = os.path.join(directory, "metadata.json")
				if os.path.isfile(metadata_path):
					metadata = json.load(open(metadata_path))
					limbs.append(metadata)
					cache[item] = metadata
		return limbs, cache

	def list_limbs(self):
		print "Limbs in {0}".format(self.metadata["name"])
		for limb in self.available_limbs:
			LimbMetadataFormatter(limb)()

		print "if you are missing a limb here, please check if there is a metadata.json in the corresponding directory!"
