#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

import json
import argparse
import os
import os.path
import sys
from collections import OrderedDict

from jsonschema import validate
import python_jsonschema_objects as pjs

import errors
from git import GitTransport
from hull import Hull
from hull_repository import HullRepository
from limb_repository import LimbRepository
from limb import Limb
from viur import Viur
from vi import Vi
from utils import fix_path, ask_procede
import logger
from report import Report, ReportDomain
from schema.config_schema import system_config_schema
from schema.project_schema import project_config_schema
from formatters.hull_format import HullMetadataFormatter
from formatters.limb_format import LimbMetadataFormatter
from formatters.config_format import ConfigMetadataFormatter


class Driver(object):
	def __init__(self, args, config=None):
		"""Driver ctor, expects a argparse result object or an object with appropriate attributes.

		No actions take place here
		"""
		super(Driver, self).__init__()
		self.report_domain = ReportDomain("driver")
		self.args = args
		self.config = config
		if config:
			self.process_config()
		self.report = None
		self.git = None
		self.hull_repository = None
		self.limb_repository = None
		self.hull = None
		self.installed_hull = None
		self.vi = None
		self.viur = None
		self.limbs = None
		self.installed_limbs = OrderedDict()

	@staticmethod
	def create_config(args):
		builder = pjs.ObjectBuilder(project_config_schema)
		ns = builder.build_classes()
		project_config = ns.Projectconfig()
		for propname in project_config.iterkeys():
			propinfo = project_config.propinfo(propname)
			ptype = propinfo["type"]
			if "default" in propinfo:
				dtype = propinfo["default"]
				if "optional" in propinfo:
					pass
				elif dtype == "list":
					setattr(project_config, propname, list())
				else:
					setattr(project_config, propname, dtype)
			else:
				if ptype == "string":
					if "enum" in propinfo:
						setattr(project_config, propname, propinfo["enum"][0])
					else:
						setattr(project_config, propname, "none")
				elif ptype == "boolean":
					setattr(project_config, propname, False)
				elif ptype == "array":
					setattr(project_config, propname, list())
		pre = project_config.serialize()
		open(args.create_project_config, "w").write(json.dumps(json.loads(pre), indent=4, sort_keys=True))

	def init(self):
		pass

	def init_report(self):
		self.report = Report.from_metadata(self.config)

	def init_git(self):
		self.git = GitTransport(self)

	def init_limb_repository(self):
		self.limb_repository = LimbRepository.from_metadata(self, self.config["limb_repository_path"])

	def init_hull_repository(self):
		self.hull_repository = HullRepository.from_metadata(self, self.config["hull_repository_path"])

	def init_hull(self):
		#: a Hull instance
		self.hull = Hull.from_file(self, self.config["hull_name"])

	def init_installed_hull(self):
		if "hull" in self.report.report:
			existing_hull_metadata = self.report.report["hull"].get("metadata", OrderedDict())
			from_repo = self.report.report["hull"].get("repository", "unknown_repository")
			self.installed_hull = Hull(self, existing_hull_metadata, existing_hull_metadata["name"])
			self.installed_hull.repository = from_repo

	def init_vi(self):
		self.vi = Vi(self)

	def init_viur(self):
		self.viur = Viur(self)

	def init_limbs(self):
		self.limbs = self.load_limbs()

	def init_installed_limbs(self):
		self.load_installed_limbs()

	def process_config(self):
		"""Sanitizes and adds important (derived) data to config"""

		self.config["hull_repository_path"] = fix_path(self.config["hull_repository_path"])
		self.config["limb_repository_path"] = fix_path(self.config["limb_repository_path"])
		self.config["path"] = fix_path(self.config["path"])
		logger.get_logger(self.config["path"])
		self.config["appengine_path"] = os.path.join(self.config["path"], "appengine")

		if self.config["user_type"] == "custom":
			self.config["user_module"] = "user_custom"
			self.config["user_module_class"] = "CustomUser"
		else:
			self.config["user_module"] = "user_google"
			self.config["user_module_class"] = "GoogleUser"

	@staticmethod
	def load_config(args):
		"""Loads, validates and merges system and project config files

		:param args: The argparse args object.
		:returns: config dict object
		"""

		try:
			system_config = json.load(open(args.config))
			validate(system_config, system_config_schema)
			project_config = json.load(open(args.project_config))
			validate(project_config, project_config_schema)
			system_config.update(project_config)
			print "done loading config"
			return system_config
		except Exception, e:
			print "Error while loading config"
			raise

	@staticmethod
	def from_metadata(args):
		"""Factory function to create a Driver from a system specific json config file
		and a project specific json config file.

		The values in the system file will be merged with the project file, the latter takes precedence.

		:param args: The argparse args object.

		.. NOTE::
		   See :data:`schema.config_schema.config_schema` for the acceptable structure

		"""

		return Driver(args)

	def load_limbs(self):
		"""Loads all Limbs requested by config

		:return: a list of existing limbs
		:rtype: :class:`list`\<:class:`viur_bootstrap.limb.Limb`\>
		"""
		return [Limb.from_file(self, limb) for limb in self.config["limbs"]]

	def load_installed_limbs(self):
		"""Loads all installed Limbs by metadata in report if existing"""
		if "installed_limbs" in self.report.report:
			for name, report in self.report.report["installed_limbs"].iteritems():
				self.installed_limbs[name] = Limb(self, report["metadata"])

	def install_limbs(self):
		"""Calls for all requested limbs its install method.

		The limb decides if it's really needed or valid.

		:return:
		"""
		for limb in self.limbs:
			if limb.metadata["name"] not in self.installed_limbs:
				self.installed_limbs[limb.metadata["name"]] = limb.install()

	def on_changed(self):
		"""See GitTransport.on_changed for more information, works on all reporter domains
		"""
		self.report.collect_reports()
		self.report.iterate_domains(self.git)
		file_list = [self.report.report_path, os.path.join(self.config["path"], "viur-bootstrap.log")]
		self.git.on_changed(file_list,
							"viur_bootstrap: adding the report and log:\n{0}\n".format(
								"\n".join(["  * {0}".format(fp) for fp in file_list])))

	def show_config(self):
		print("used config:")
		ConfigMetadataFormatter(self.config)()

	def show_installed_limbs(self):
		for name, limb in self.installed_limbs.iteritems():
			LimbMetadataFormatter(limb.metadata)()

	def show_config_limbs(self):
		for limb in self.limbs:
			LimbMetadataFormatter(limb.metadata)()

	def show_hull(self, hull=None):
		HullMetadataFormatter(hull.metadata)()

	def show_installed_hull(self):
		print "Installed hull metadata:"
		self.show_hull(self.installed_hull)

	def show_config_hull(self):
		print "Hull metadata:"
		self.show_hull(self.hull)

	def list_limbs(self):
		self.limb_repository.list_limbs()

	def dispatch(self):
		if self.args.config and self.args.project_config:
			not_conf = self.config is None
			self.config = self.load_config(self.args)
			if not_conf:
				self.process_config()

		if self.args.show_config:
			self.show_config()
		elif self.args.search_limbs:
			self.init_limb_repository()
			self.limb_repository.search(self.args.search_limbs)
		elif self.args.list_limbs:
			self.init_report()
			self.init_limb_repository()
			self.init_limbs()
			self.list_limbs()
		elif self.args.list_hulls:
			self.init_report()
			self.init_hull_repository()
			self.hull_repository.list_hulls()
		elif self.args.show_limbs:
			self.init_report()
			self.init_limbs()
			self.show_config_limbs()
		elif self.args.show_installed_limbs:
			self.init_report()
			self.init_installed_limbs()
			self.show_installed_limbs()
		elif self.args.show_hull:
			self.init_report()
			self.init_hull()
			self.show_config_hull()
		elif self.args.show_installed_hull:
			self.init_report()
			self.init_installed_hull()
			self.show_installed_hull()
		elif self.args.create_project_config:
			self.create_config(self.args)
		else:
			self.bootstrap()

	def bootstrap(self):
		self.init_report()
		self.init_git()
		self.init_hull_repository()
		self.init_hull()
		self.init_installed_hull()
		self.init_limb_repository()
		self.init_limbs()
		self.init_vi()
		self.init_viur()
		self.show_config()
# 		prompt = """This will initialize the application '{app_id}' in '{path}' using hull '{hull_name}'.
# Continue [y/n]?\n""".format(**self.config)
# 		ask_procede(prompt)
		self.git.init(self.config["path"])
		self.git.add_remote(self.config["git_remote"], self.config["path"])
		try:
			if not self.installed_hull or self.installed_hull != self.hull:
				self.hull.install()
		except errors.HullAlreadyInstalled:
			ask_procede(
				"""Trying to boostrap into in already existing viur instance. Did you want to update?
Exiting now...Press y for yes\n""")
		self.git.add_git_ignore()
		self.vi.add()
		self.viur.add()
		self.install_limbs()
		self.vi.build()
		self.on_changed()


def main():
	parser = argparse.ArgumentParser(prog="viur-bootstrap", description='bootstrapping a new viur server instance')
	parser.add_argument('-c', '--config', default=os.path.expanduser("~/viur_bootstrap.json"),
						help='the system specific json config file, default=os.path.expanduser("~/viur_bootstrap.json")')
	parser.add_argument('-p', '--project-config',
						help='the project specific json config file. This is required!')
	parser.add_argument('-P', '--create-project-config',
						help='create a new project config at provided location')
	group = parser.add_mutually_exclusive_group()
	group.add_argument('-u', '--update', action="store_true",
					   help='updates an existing viur application instance')
	group.add_argument('-l', '--limb',
					   help='explicitly installs a limb into existing viur application instance')
	parser.add_argument('-C', '--show-config', action="store_true",
						help='shows the metadata of the config')
	parser.add_argument('-L', '--show-limbs', action="store_true",
						help='shows the metadata of all limbs to install')
	parser.add_argument('-z', '--list-limbs', action="store_true",
						help='shows the metadata of all limbs in repository')
	parser.add_argument('-Z', '--list-hulls', action="store_true",
						help='shows the metadata of all hulls in repository')
	parser.add_argument('-s', '--search-limbs',
						help='shows the metadata of all limbs in repository')
	parser.add_argument('-I', '--show-installed-limbs', action="store_true",
						help='shows the metadata of all installed limbs')
	parser.add_argument('-H', '--show-hull', action="store_true",
						help='shows the metadata of the hull')
	parser.add_argument('-J', '--show-installed-hull', action="store_true",
						help='shows the metadata of the installed hull')

	args = parser.parse_args(sys.argv[1:])

	driver = Driver(args)
	driver.dispatch()


if __name__ == '__main__':
	main()
