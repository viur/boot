# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'
import os.path
import json

from jsonschema import validate

from logger import logger
from schema.hull_repository_schema import hull_repository_schema
from formatters.hull_format import HullMetadataFormatter


class HullRepository(object):
	"""

	"""

	def __init__(self, parent, metadata):
		super(HullRepository, self).__init__()
		self.parent = parent
		self.metadata = metadata
		self.config = parent.config
		self.path = parent.config["hull_repository_path"]
		self.available_hulls, self.cache = self.retrieve_hulls()

	@staticmethod
	def from_metadata(parent, path):
		try:
			hull_repository_metadata = json.load(open(os.path.join(path, "metadata.json")))
			validate(hull_repository_metadata, hull_repository_schema)
			return HullRepository(parent, hull_repository_metadata)
		except Exception, e:
			raise

	def sync(self):
		if self.metadata["git_url"]:
			logger.info("updating hull repository %r", self.metadata["name"])
			self.parent.git.pull(self.path)
		else:
			logger.info("skipping hull repository %r - no upstream url specified", self.metadata["name"])

	def retrieve_hulls(self):
		hulls = list()
		cache = dict()
		for item in os.listdir(self.path):
			directory = os.path.join(self.path, item)
			if os.path.isdir(directory):
				metadata_path = os.path.join(directory, "metadata.json")
				if os.path.isfile(metadata_path):
					metadata = json.load(open(metadata_path))
					hulls.append(metadata)
					cache[item] = metadata
		return hulls, cache

	def list_hulls(self):
		print "Hulls in hull repository '{0}'".format(self.metadata["name"])
		for hull in self.available_hulls:
			HullMetadataFormatter(hull)()

		print "if you are missing a hull here, please check if there is a metadata.json in the corresponding directory!"
