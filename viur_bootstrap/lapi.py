# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

import os
import os.path
from datetime import datetime
from distutils.dir_util import copy_tree
from distutils.file_util import copy_file
from distutils import errors as dist_errors

from jsonschema import validate

import logger
from schema.limb_schema import limb_schema_1
from utils import edit_python_src, write_imports
from report import ReportDomain


class LAPIHandler1(object):
	"""Defines the feature set of LAPI Version 1

	"""

	def __init__(self, driver, limb):
		super(LAPIHandler1, self).__init__()
		self.config = driver.config
		self.plugin_path_dst = os.path.join(self.config["path"], "vi_customizing", "vi_plugins")
		self.driver = driver
		self.limb = limb
		self.metadata = limb.metadata
		if limb.dir_name:
			self.limb_root = os.path.abspath(os.path.join(self.config["limb_repository_path"], limb.dir_name))
		else:
			self.limb_root = None
		self.report_domain = ReportDomain(self.metadata["name"], "installed_limbs")
		driver.report.register_domain_handler(self.metadata["name"], self)

	def validate_metadata(self):
		validate(self.metadata, limb_schema_1)

	def add_key_directories(self, key):
		try:
			data = self.metadata[key]
			for dirname in data:
				src = os.path.abspath(os.path.join(self.limb_root, "appengine", dirname))
				self.report_domain.add_installed_files(copy_tree(src, os.path.abspath(
					os.path.join(self.config["appengine_path"], dirname))))
		except dist_errors.DistutilsFileError:
			return

	def add_bones(self):
		"""Adds new bones to subclasses of skeletons

		We do not add skeletons to an instance, but append new bones

		We do not check if there bones are already existing.

		.. TODO::
		   implement a feature to check upfront, if its needed to install these bones, or replace older installations by
		   this kind.
		"""
		src_path = os.path.join(self.limb_root, "appengine", "models")
		dst_path = os.path.join(self.config["appengine_path"], "models")
		code_per_skel = self.metadata["add_bones"]
		if not code_per_skel:
			return

		for skel in code_per_skel:
			src_skel = os.path.join(src_path, skel)
			dst_skel = os.path.join(dst_path, skel)
			dst_lines = open(dst_skel).readlines()
			src_lines = open(src_skel).readlines()
			new_lines = edit_python_src("# MEMBER_MARKER", dst_lines, src_lines)
			open(dst_skel, "w").writelines(new_lines)
			self.report_domain.add_installed_files(dst_skel)

	def add_templates(self):
		self.add_key_directories("template_dirs")

	def add_static_dirs(self):
		self.add_key_directories("static_dirs")

	def add_libs(self):
		self.report_domain.add_installed_files(
			copy_tree(os.path.abspath(os.path.join(self.limb_root, "appengine", "libs")),
			          os.path.abspath(
				          os.path.join(self.config["appengine_path"], "libs"))))

	def add_files(self, key, only_modules=False):
		src_root = os.path.abspath(os.path.join(self.limb_root, "appengine", key))
		dst_root = os.path.abspath(os.path.join(self.config["appengine_path"], key))
		import_list = list()
		init_file = os.path.abspath(os.path.join(dst_root, "__init__.py"))
		for item in self.metadata[key]:
			src = os.path.abspath(os.path.join(src_root, item["file"]))
			dst = os.path.abspath(os.path.join(dst_root, item["file"]))
			self.report_domain.add_installed_files(copy_file(src, dst)[0])
			if only_modules:
				import_list.append(item["file"][:-3])
			else:
				import_list.append([item["file"][:-3], item["class"]])

		write_imports(init_file, import_list, only_modules)
		self.report_domain.add_installed_files(init_file)

	def add_skeletons(self):
		self.add_files("models", True)

	def add_modules(self):
		self.add_files("modules")

	def add_vi_plugins(self):
		try:
			os.makedirs(self.plugin_path_dst)
		except OSError:
			pass

		dst_path = os.path.join(self.config["path"], "vi_customizing", "vi_plugins", "__init__.py")
		init_file = open(dst_path, "w+")
		for plugin in os.listdir(os.path.join(self.limb_root, "vi_plugins")):
			logger.logger.info("Installing vi plugin %r", plugin)
			src = os.path.join(self.limb_root, "vi_plugins", plugin)
			if os.path.isdir(src):
				dst = os.path.join(self.plugin_path_dst, plugin)
				self.report_domain.add_installed_files(copy_tree(src, dst))
				init_file.write("import {0}\n".format(plugin))  # quick and dirty here :)
		os.symlink(self.plugin_path_dst, os.path.join(self.config["path"], "vi", "vi_plugins"))
		self.report_domain.add_installed_files(dst_path)

	def add_admin_plugins(self):
		pass

	def collect_report(self):
		"""This is a callback method for an Report instance.

		If you want to provide more information, you should overwrite this method in your subclass

		:return: a tuple with the key, the report data and if this mixin is a limb
		:rtype: str, dict, bool
		"""
		self.metadata["updated"] = datetime.now().strftime("%Y.%m.%d %H:%M")
		self.metadata.setdefault("created", self.metadata["updated"])
		self.metadata["repository"] = self.driver.limb_repository.metadata["name"]
		return self.report_domain.key, {"installed_files": self.report_domain.installed_files,
		                                "metadata": self.metadata}, self.report_domain.section

	def install(self):
		self.add_templates()
		self.add_skeletons()
		self.add_modules()
		self.add_bones()
		self.add_vi_plugins()
		self.add_admin_plugins()
		self.add_static_dirs()
		self.add_libs()
		return self.metadata
