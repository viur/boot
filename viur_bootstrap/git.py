# -*- coding: utf-8 -*-

__author__ = 'Stefan Kögl'


import time
import subprocess
import re
import os
import os.path
from distutils.file_util import copy_file

import logger
import errors
from report import ReportDomain


class GitTransport(object):
	def __init__(self, driver):
		"""

		:param driver: Driver
		:type driver: Driver
		"""
		super(GitTransport, self).__init__()
		self.report_domain = ReportDomain("git")
		self.config = driver.config
		driver.report.register_domain_handler("git", self)
		self.git_path = self.config.get("git_path", False)
		self.commit_on = self.config.get("git_commit", False)
		self.push_on = self.config.get("git_push", False)
		self.remote_on = self.config.get("git_remote")
		self.init_on = self.config.get("git_init", False)
		self.dry_run = self.config.get("dry_run", False)

		if self.git_path and not os.path.isfile(self.git_path):
			self.git_path = None

		if not (self.commit_on or self.push_on) and self.git_path:
			raise errors.GitNotFoundError("git binary not found or specified in config")

		self.has_repository = self.check_for_git()
		self.remotes = set()
		self.has_remotes = self.get_remotes(self.config["path"])

	def check_for_git(self):
		"""This method decides if there is support for git commands,
		 which depends on existing git repository in path.

		:return: bool
		"""
		res = os.path.isdir(os.path.join(self.config["path"], ".git"))
		self.has_repository = self.config["is_git_repo"] = res
		return res

	def init(self, cwd):
		if not self.has_repository and self.init_on:
			logger.logger.info("init git repository in path %r...", cwd)
			subprocess.Popen("{0} init".format(self.git_path), cwd=cwd,
									shell=True, stdout=subprocess.PIPE)
		self.has_repository = self.config["is_git_repo"] = True
		# hopefully fixes race condition with changes to filesystem have not synced before further actions takes place
		time.sleep(3)

	def add_git_ignore(self):

		if self.has_repository:
			dst_path = copy_file(os.path.abspath(os.path.join(os.path.dirname(__file__), ".gitignore")),
								 os.path.abspath(os.path.join(self.config["path"], ".gitignore")))[0]
			logger.logger.info("add_git_ignore %r", dst_path)
			self.report_domain.installed_files.append(dst_path)

	def get_remotes(self, cwd):
		"""Retrieves all git remote repositories.

		A set of remote repository urls is stored into GitTransport.remotes

		:param cwd: the root directory of our repository
		:type cwd: str
		:return: True if there are remote repos or False if not
		"""

		# print "get_Remotes", self.has_repository
		if self.has_repository:
			remote_raw = subprocess.check_output("{0} remote -v".format(self.git_path), shell=True, cwd=cwd)
			# print "remote_raw"
			# print repr(remote_raw)
			# print
			m_fetch = re.compile("origin\t(.*?) \(fetch\)")
			m_push = re.compile("origin\t(.*?) \(push\)")
			self.remotes.update(m_fetch.findall(remote_raw))
			self.remotes.update(m_push.findall(remote_raw))
		# print "remotes", self.remotes
		return self.remotes and True or False

	def add_remote(self, remote, cwd):
		"""Adds a git remote url for a upstream repository to our repository if it does not exist

		:param remote: the remote url
		:type remote: str
		:param cwd: the root directory of our repository
		:type cwd: str
		"""
		if self.has_repository and self.remote_on and remote not in self.remotes:
			logger.logger.info("add git remote %r to path %r...", remote, cwd)
			subprocess.check_output("{0} remote add origin {1}".format(self.git_path, remote),
									cwd=cwd, shell=True)
			self.remotes.add(remote)
		else:
			logger.logger.info("skipping to add existing remote %r to path %r...", remote, cwd)
		self.remote_on = True

	def add(self, file_name, cwd):
		"""

		:param file_name: the file or directory to add to git stage
		:type file_name: str
		:return:
		"""
		if self.has_repository and self.commit_on:
			logger.logger.info("add %r in path %r...", file_name, cwd)
			cmd = "{0} add -f {1}".format(self.git_path, file_name)
			try:
				subprocess.check_output(cmd,
										cwd=cwd, shell=True)
			except subprocess.CalledProcessError, err:
				logger.logger.info("git add went wrong: %r with err code %r", err.output, err.returncode)
				logger.logger.exception(err)

	def add_submodule(self, url, cwd):
		"""Adds a git submodule specified as argument 'url' under current working directory 'cwd'

		e.g:
			git submodule add -f "http://localhost/amazing_app.git

		If you don't know about git submodules, please consult the git manual and/or the internet...

		:param url: an url to create the submodule from
		:type url: str
		:param cwd: the directory to register the submodule
		:type cwd: str
		:return:
		"""
		if self.has_repository:
			logger.logger.info("adding %r to git submodules under %r...", url, cwd)
			subprocess.check_output("{0} submodule add -f {1}".format(self.git_path, url),
									cwd=cwd, shell=True)

	def use_submodules(self, cwd):
		"""activates submodules if specified in config

		:param cwd: the current git repository to use
		:type cwd: str
		"""

		if self.has_repository:
			logger.logger.info("initialize submodules in path %s...", cwd)
			subprocess.check_output("{0} submodule init".format(self.git_path), cwd=cwd,
									shell=True)
			subprocess.check_output("{0} submodule update".format(self.git_path), cwd=cwd,
									shell=True)

	def commit(self, message=None):
		"""Commits the recent stage to our git repository
		"""
		if self.has_repository and self.commit_on:
			if message is None:
				message = 'viur_bootstrap: {0!r} engaged by viur_bootstrap'.format(self.config["app_id"])
			logger.logger.info("committing everything...")
			try:
				subprocess.check_output(
					'{0} commit -m "{1}"'.format(self.git_path, message),
					cwd=self.config["path"], shell=True)
			except subprocess.CalledProcessError, err:
				if err.returncode == 1:
					pass
				else:
					raise

	def push(self):
		"""Pushes the recent HEAD to origin if it exists
		"""
		if self.has_repository and self.remote_on and self.push_on:
			logger.logger.info("setting upstream branch...")
			subprocess.check_output("push --set-upstream origin master".format(self.git_path), cwd=self.config["path"],
									shell=True)
			logger.logger.info("pushing commit upstream...")
			subprocess.check_output("{0} push".format(self.git_path), cwd=self.config["path"],
									shell=True)

	def on_changed(self, installed_files, message=None):
		"""Method to call after a hull or limb was touched and calling
		 git add, git commit and git push actions

		The actions I perform are:
			* add installed files to git stage
			* committing the stage
			* push the commit to origin if existing

		:param installed_files: a list of absolute file paths
		:type installed_files: :class:`list`\<:class:`str`\>
		"""

		path = self.config["path"]
		for file_name in installed_files:
			self.add(file_name, path)
		self.commit(message)
		self.push()

	def switch_branch(self, branch_name, cwd):
		"""Switches a branch to branch_name, but only if differs from master


		:param branch_name: the branch_name to switch to
		:type branch_name: str
		:param cwd: the directory in which the git repository resides
		:type cwd: str
		"""

		if branch_name != "master":
			logger.logger.info("switching git branch to {0} in repository {1}".format(branch_name, cwd))
			subprocess.check_output("{0} checkout {1}".format(self.git_path, branch_name), cwd=cwd, shell=True)

	def clone(self, url, cwd):
		"""Clones git repo by url into directory cwd

		:param url: the git repository origin
		:type url: :class:`str`
		:param cwd: the directory to clone into
		:type cwd: :class:`str`
		"""
		logger.logger.info("cloning %r into %r", url, cwd)
		cmd = "{0} clone {1}".format(self.git_path, url)
		subprocess.check_output(cmd, cwd=cwd,
								shell=True)

	def pull(self, name, cwd):
		"""Pulls aka updates a git repo which resides in cwd

		:param str name: the name of the git repository, not really needed but nice to have info for the log
		:param str cwd: the directory of the repo
		"""
		logger.logger.info("pulling %r into %r", name, cwd)
		subprocess.Popen("{0} pull".format(self.git_path), cwd=cwd, shell=True, stdout=subprocess.PIPE)
