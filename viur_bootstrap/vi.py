# -*- coding: utf-8 -*-
__author__ = 'stefan'

import subprocess
import os
import os.path
import sys
import logger
from report import ReportDomain


class Vi(object):
	def __init__(self, driver):
		"""

		:param driver: our master control program
		:type driver: :class:`viur_bootstrap.driver.Driver`
		"""
		super(Vi, self).__init__()

		self.report_domain = ReportDomain("vi")
		driver.report.register_domain_handler("vi", self)
		self.driver = driver
		self.config = driver.config
		self.vi_add = self.config.setdefault("vi_add", False)
		self.vi_build = self.config["vi_build"]
		self.vi_path = os.path.join(self.config["path"], "vi")
		self.added = self.exists()
		self.git = driver.git

	def exists(self):
		return os.path.isdir(self.vi_path)

	def add(self):
		if self.vi_add and not self.added:
			vi_path = os.path.join(self.config["path"], "vi")
			logger.logger.info("adding vi into %r...", self.config["path"])
			if self.config["is_git_repo"] and self.config["vi_submodule"]:
				self.git.add_submodule(self.config["vi_url"], self.config["path"])
				self.report_domain.add_installed_files(vi_path)
			else:
				self.git.clone(self.config["vi_url"], self.config["path"])

			self.git.switch_branch(self.config["vi_branch"], vi_path)
			# self.git.use_submodules(vi_path)
			self.added = True

	def build(self):
		if self.added and self.vi_build:
			if sys.platform in ["linux", "linux2"]:
				self.build_linux()
			else:
				logger.logger.warning("Other platforms than linux are not yet implemented to build vi.")

	def build_linux(self):
		logger.logger.info("compile vi using provided makefile...")
		subprocess.Popen("make setup", stderr=subprocess.STDOUT, cwd=self.vi_path, shell=True).communicate()
		subprocess.Popen("make deploy", stderr=subprocess.STDOUT, cwd=self.vi_path, shell=True).communicate()
