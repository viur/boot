# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

from format_base import *


class HullMetadataFormatter(MetadataFormatter):
	def __call__(self, color=True):
		print self._colorize(self.metadata["name"], BLUE, color)
		print self._colorize("    version: ".ljust(self.intend), INDIGO, color), self.metadata["version"]
		if "created" in self.metadata:
			print "    created".ljust(self.intend), self.metadata["created"]
		if "updated" in self.metadata:
			print "    updated".ljust(self.intend), self.metadata["updated"]
		if "repository" in self.metadata:
			print "    repository".ljust(self.intend), self.metadata["repository"]
		print
