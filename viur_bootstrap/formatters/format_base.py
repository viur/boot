# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

from textwrap import TextWrapper
import copy


RED = "31;1m"
GREEN = "32;1m"
YELLOW = "33;1m"
BLUE = "34;1m"
MAGENTA = "35;1m"
INDIGO = "36;1m"
WHITE = "0;1m"

__all__ = ["NameVersionCombiner", "MetadataFormatter", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "INDIGO", "WHITE"]


class NameVersionCombiner(object):
	def __init__(self, a, b, separator):
		"""

		:param a:
		:type a: str
		:param b:
		:type b: str
		:param separator:
		:type separator: str
		"""
		self.a = a
		self.b = b
		self.separator = separator

	def __call__(self, metadata):
		"""

		:param metadata:
		:type metadata: dict
		:return: list, dict
		"""
		av = metadata[self.a]
		bv = metadata[self.b]
		return "%s%s%s" % (av, self.separator, bv)


class MetadataFormatter(object):
	def __init__(self, metadata):
		self.metadata = metadata
		self.width = 80
		self.intend = 25
		col_intend = self.intend + 1
		self._wrapper = TextWrapper(width=self.width, initial_indent=" " * col_intend,
		                            subsequent_indent=" " * col_intend)
		self._first_wrapper = TextWrapper(width=self.width - self.intend, subsequent_indent=" " * col_intend)

	def _colorize(self, text, color=WHITE, use_color=True):
		if use_color:
			return "\033[%s%s\033[0;0m" % (color, text)
		else:
			return text

	def _indent(self, text, indent=4):
		return "%s%s" % ("" * indent, text)

	def _show_text(self, key):
		descr = self.metadata[key].split("\\n")
		print "   ", key.capitalize().ljust(self.intend - 4),
		descr_first = descr.pop(0)
		for line in self._first_wrapper.wrap(descr_first):
			if not line:
				print
			for col_line in self._first_wrapper.wrap(line):
				print col_line
		for line in descr:
			if not line:
				print
			for col_line in self._wrapper.wrap(line):
				print col_line

	def _show_dict_list(self, key, combiner):
		print "   ", key.capitalize().ljust(self.intend)
		for item in self.metadata[key]:
			print " " * self.intend, combiner(item)
			for key, value in item.iteritems():
				if key == combiner.a or key == combiner.b:
					continue
				else:
					print " " * (self.intend + 4), "%s: %s" % (key, value)

	def _show_list(self, key):
		data = copy.copy(self.metadata[key])
		if not data:
			print ("    %s" % key.capitalize()).ljust(self.intend), "[]"
			return
		first = data.pop()
		print ("    %s" % key.capitalize()).ljust(self.intend), first
		for item in data:
			print " " * self.intend, item
