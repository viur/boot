# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

from format_base import *


class LimbMetadataFormatter(MetadataFormatter):
	def __call__(self, color=True):
		print self._colorize("* %s-%s" % (self.metadata["name"], self.metadata["version"]), BLUE, color)
		if "created" in self.metadata:
			print "    created".ljust(self.intend), self.metadata["created"]
		if "updated" in self.metadata:
			print "    updated".ljust(self.intend), self.metadata["updated"]
		if "repository" in self.metadata:
			print "    repository".ljust(self.intend), self.metadata["repository"]
		self._show_text("description")
		print self._colorize("    LAPI".ljust(self.intend), INDIGO, color), self.metadata["LAPI"]
		print "    Skeletons".ljust(self.intend)
		for skel in self.metadata["models"]:
			print " " * self.intend, "%s:%s" % (skel["file"], skel["class"])
		print "    Modules".ljust(self.intend)
		for skel in self.metadata["modules"]:
			print " " * self.intend, "%s:%s" % (skel["file"], skel["class"])
		self._show_dict_list("libs", NameVersionCombiner("name", "version", "-"))
		self._show_list("required_skeletons")
		self._show_list("required_modules")
		print
		print


if __name__ == '__main__':
	import json

	metadata = json.load(open("resources/limb_repository/qr/metadata.json"))
	LimbMetadataFormatter(metadata)()
