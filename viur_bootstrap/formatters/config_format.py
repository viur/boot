# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

from format_base import *


class ConfigMetadataFormatter(MetadataFormatter):
	def __call__(self, color=True):
		data = self.metadata.copy()
		print self._colorize("* %s" % data.pop("app_id"), BLUE, color)

		keys = sorted(data.keys())
		for key in keys:
			value = data[key]
			if isinstance(value, list):
				self._show_list(key)
			else:
				print ("    %s" % key).ljust(self.intend), value
