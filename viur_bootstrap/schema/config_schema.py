# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

#: This dict represents the json schema to validate a viur_bootstrap config json file
system_config_schema = {
	"title": "config",
	"type": "object",
	"required": ["git_path"],
	"properties": {
		"git_path": {"type": "string"},
		"lessc_path": {"type": "string"},
		"pyjs_path": {"type": "string"},
	}
}
