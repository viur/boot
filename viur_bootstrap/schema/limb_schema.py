# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

#: This dict represents the json schema to validate a limb metadata version 1
limb_schema_1 = {
	"type": "object",
	"required": ["name", "LAPI", "version", "authors"],
	"properties": {
		"edited": {"type": "string"},
		"updated": {"type": "string"},
		"repository": {"type": "string"},
		"name": {"type": "string"},
		"LAPI": {"type": "integer"},
		"description": {"type": "string"},
		"authors": {"type": "array", "items": {"type": "string"}},
		"warning": {"type": "string"},
		"version": {"type": "string"},
		"viur_branch": {"type": "string"},
		"vi_branch": {"type": "string"},
		"required_modules": {"type": "array", "items": {"type": "string"}},
		"required_skeletons": {"type": "array", "items": {"type": "string"}},
		"add_bones": {"type": "array", "items": {"type": "string"}},
		"models": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"file": {"type": "string"},
					"class": {"type": "string"}
				}
			}
		},
		"modules": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"file": {"type": "string"},
					"class": {"type": "string"}
				}
			}
		},
		"files": {"type": "array", "items": {"type": "string"}},
		"libs": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"name": {"type": "string"},
					"version": {"type": "string"},
					"homepage": {"type": "string"}
				}
			}
		},
		"vi_plugins": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"name": {"type": "string"},
					"description": {"type": "string"}
				}
			}
		},
		"required_limbs": {"type": "array", "items": {"type": "string"}},
		"template_dirs": {"type": "array", "items": {"type": "string"}},
		"static_dirs": {"type": "array", "items": {"type": "string"}},
	}
}
