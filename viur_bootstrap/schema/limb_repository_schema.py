# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

#: This dict represents the json schema to validate a hull metadata
limb_repository_schema = {
	"type": "object",
	"required": ["name", "description"],
	"properties": {
		"name": {"type": "string"},
		"description": {"type": "string"}
	}
}
