# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

#: This dict represents the json schema to validate a viur_bootstrap project config json file
project_config_schema = {
	"title": "ProjectConfig",
	"type": "object",
	"required": ["path", "authors", "hull_name", "app_id", "app_name"],
	"properties": {
		"app_id": {"type": "string", "default": "bimmel-bammel-viur"},
		"app_name": {"type": "string", "default": "Bimmel Bammel"},
		"authors": {"type": "array", "items": {"type": "string"}, "default": "list"},
		"user_type": {
			"type": "string",
			"enum": ["custom", "google"],
			"default": "google"
		},
		"path": {
			"type": "string",
			"description": "the directory we will use for our viur application instance",
		},
		"git_commit": {"type": "boolean", "default": True},
		"git_push": {"type": "boolean", "default": False},
		"git_init": {"type": "boolean", "default": True},
		"git_remote": {"type": "string", "default": "git@bitbucket.org:yourname/bimmel-bammel-viur.git"},
		"viur_add": {"type": "boolean", "default": True},
		"viur_url": {"type": "string", "default": "https://bitbucket.org/viur/server.git"},
		"viur_branch": {"type": "string", "default": "master"},
		"viur_submodule": {"type": "boolean", "default": True},
		"vi_add": {"type": "boolean", "default": True},
		"vi_build": {"type": "boolean", "default": False},
		"vi_url": {"type": "string", "default": "https://bitbucket.org/viur/vi.git"},
		"vi_branch": {"type": "string", "default": "master"},
		"vi_submodule": {"type": "boolean", "default": True},
		"hull_repository_path": {"type": "string", "default": "~/IdeaProjects/viur_bootstrap/viur_bootstrap/resources/hull_repository", "optional": True},
		"limb_repository_path": {"type": "string", "default": "~/IdeaProjects/viur_bootstrap/viur_bootstrap/resources/limb_repository", "optional": True},
		"hull_name": {"type": "string", "default": "base_hull"},
		"limbs": {
			"type": "array",
			"items": {"type": "string"}
		}
	}
}
