# -*- coding: utf-8 -*-
__author__ = 'Stefan Kögl'

#: This dict represents the json schema to validate a hull metadata
hull_schema = {
	"type": "object",
	"required": ["name", "version", "authors", "renaming_targets", "template_targets"],
	"properties": {
		"edited": {"type": "string"},
		"updated": {"type": "string"},
		"repository": {"type": "string"},
		"name": {"type": "string"},
		"version": {"type": "string"},
		"authors": {"type": "array", "items": {"type": "string"}},
		"renaming_targets": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"src_name": {"type": "string"},
					"dst_fmt": {"type": "string"},
					"src_args": {"type": "array", "items": {"type": "string"}},
				}
			}
		},
		"template_targets": {
			"type": "array",
			"items": {"type": "string"}
		}
	}
}
