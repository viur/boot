# -*- coding: utf-8 -*-
__author__ = 'stefan'

import sys
import itertools
import os.path
import json

from jsonschema import validate


def fix_path(path):
	"""Returns a absolute path. The input path can also be a

	Technically it's a convenience function for abspath(expenduser())

	:param path: the path to sanitize
	:type path: str
	:return: the sanitized path
	:rtype: str
	"""

	return os.path.abspath(os.path.expanduser(path))


def write_imports(dst_file, import_list=None, only_modules=False):
	"""Adds needed import statements into a python file

	:param dst_file:
	:param import_list:
	:param only_modules:
	:return:
	"""
	text = open(dst_file, "r").read().rstrip("\n").split("\n")
	header = list()
	imports = list()
	from_imports = list()
	for line in text:
		if line.startswith("#"):
			header.append(line)
		elif line.startswith("__author__"):
			header.append(line)

		if line.startswith("from "):
			from_imports.append(line)
		if line.startswith("import"):
			imports.append(line)

	if only_modules:
		imports.extend(["import %s" % module_name for module_name in import_list])
		imports = sorted(list(set(imports)))
	else:
		from_imports.extend(
			["from %s import %s" % (file_name, class_name) for file_name, class_name in import_list])
		from_imports = sorted(list(set(from_imports)))

	open(dst_file, "w").write("%s\n\n%s\n\n%s\n" % ("\n".join(header), "\n".join(imports), "\n".join(from_imports)))


def edit_python_src(marker, src_lines, add_lines):
	"""Inserts new code into an existing file

	The new content will be inserted exactly one line before
	the marker with the same indention level as our marker.

	The marker should have the form of a python comment e.g:
	# SWAG_MARKER

	:param marker:
	:type marker: str
	:param src_lines: a list of strings as given by open().readlines() or equivalent with newlines as last character
	:type src_lines: :class:`list`\<:class:`str`\>
	:param add_lines: the new content also as lines ending with newline
	:type add_lines: :class:`list`\<:class:`str`\>
	:return: the new file as lines
	:rtype: :class:`list`\<:class:`str`\>
	"""
	s_indent = None
	indent_char = " "
	for ix, i in enumerate(src_lines):
		if not s_indent:
			try:
				s_indent = i.index(marker)
				if i[s_indent - 1] == "\t":
					indent_char = "\t"
				s_data_before = src_lines[:ix]
				s_data_after = src_lines[ix:]
				indenter = indent_char * s_indent
				src_lines = "".join(
					itertools.chain(s_data_before, ["%s%s" % (indenter, line) for line in add_lines], ["\n"],
					                s_data_after))
			except ValueError:
				# print err
				pass
	return src_lines


def ask_procede(prompt):
	if raw_input(prompt).lower() not in ["y", "yes"]:
		sys.exit(0)


def load_and_validate_metadata(path, schema):
	metadata = json.load(open(path))
	validate(metadata, schema)
	return metadata

def replace_keywords(filename, **kwargs):
	content = open(filename).read()
	for k, v in kwargs.items():
		content = content.replace("{{%s}}" % str(k), str(v))

	open(filename, "w").write(content)
