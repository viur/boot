A simple system for creating usable and customizable viur instances with usage of base templates and optional packages/modules/features to drop in.

* vb_root = the dest path
* vb_transport = the tool or protocol to move or install files from src to dst
* vb_files = file objects on operating system level, can also be a pipe etc
* vbt prefix = viur bootstrap templating system
* vbt_filename = a filename which consists of a plain valid filename or new style python string formatted template which results in a valid filename
* vbt_content = the content of  which consists of a plain valid filename or new style python string formatted template which results in a valid filename
* vbt_all = includes vbt_filename and vbt_content
* git integration
* viur libs for an viur server instance

* base template - a self sustained version of viur with every bells and wristles needed to fire up and/or deploy to gae
    * every filename and file content is subject to templating new style string formatting
    with arguments condensed from cli arguments, every viur cells config arguments or other sources not yet specified here

* viur cells - applications or modules which are optional or mandatory for a base template but a single selection and provides the same functionality
    * files to transport
    * modify vbt_all
    * has dependencies on other cells

* vi cells - plugins and patches which modifies the functionality, design or behaviour of the html admin tool
    * files to transport
    * modify vbt_all
    * has dependencies on other cells

* viur server related questions
    * version/branch
    * installation method
    * patches to apply

* vi related questions
    * version/branch
    * installation method
    * patches to apply
    * plugins? -> cell

* bt_transport - file transport / installation method
    * git
    * rsync
    * single file copy
    * shutil
    * trace_cp

* helper tools
    * template db admin tool
        * create a new template
        * clone
        * modify
    * cell db admin tool
        * create
        * clone
        * modify

* viur cell repository
    * easy cell listing and traversal
    * cell categories
    * cell metadata
        * checksums
        * metadata version
        * name
        * descr
        * tags
        * options
        * viur_branch min/max
        * dependencies
            * viur cell dependency spec 0.1
        * patches

* vi cell repository
    * easy cell listing and traversal
    * cell metadata
        * checksums
        * metadata version
        * name
        * descr
        * tags
        * options
        * vi_branch min/max
        * dependencies
            * vi cell dependency spec 0.1
        * patches

* viur lib repository
    * easy remote version, security and patch tracking
    * easy to use repackaging tools
        * possibly needs a bot to grep for new upstream package versions, pack and deploy it
    * lib metadata
        * checksums
        * metadata version
        * name
        * descr
        * tags
        * options
        * viur_branch min/max
        * dependencies
            * viur lib dependency spec 0.1
        * patches
