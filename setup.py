#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

from setuptools import find_packages, setup


extras = dict()

if sys.version_info >= (3,):
	extras['use_2to3'] = True

setup(
	name='viur_bootstrap',
	version='0.1',
	packages=find_packages(),
	install_requires=["jsonschema", "python_jsonschema_objects"],
	zip_safe=False,
	entry_points="""
    [console_scripts]
        viur_bootstrap = viur_bootstrap.driver:main
	""",
	author=u"Stefan Kögl <sk@mausbrand.de>",

	author_email="sk@mausbrand.de",
	description="a tool to easily initialize viur application instances and manage additional features",
	license="GPL v3",
	keywords="",
	url="",
)
